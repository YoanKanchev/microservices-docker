package com.myproject.notification;

import com.myproject.clients.notification.NotificationRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public record NotificationService(NotificationRepository notificationRepository) {

    public Notification sendNotification(NotificationRequest notificationRequest) {

        Notification notification = Notification
                .builder()
                .customerId(notificationRequest.customerId())
                .message(notificationRequest.message())
                .sendTo(notificationRequest.toCustomerEmail())
                .sender("YoanKanchev")
                .sendDate(LocalDateTime.now())
                .build();

        return notificationRepository.saveAndFlush(notification);
    }
}
