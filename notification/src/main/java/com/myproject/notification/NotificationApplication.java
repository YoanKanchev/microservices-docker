package com.myproject.notification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {
        "com.myproject.notification",
        "com.myproject.amqp"
})
public class NotificationApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotificationApplication.class, args);
    }

//    @Bean
//    CommandLineRunner commandLineRunner(RabbitMQMessageProducer producer, NotificationConfig config) {
//        return args -> {
//            producer.publish(
//                    new Person("Ali", 18),
//                    config.getInternalExchange(),
//                    config.getInternalNotificationRoutingKey()
//            );
//        };
//    }
//
//    record Person(String name, int age) {
//
//    }
}
