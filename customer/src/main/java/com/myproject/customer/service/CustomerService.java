package com.myproject.customer.service;

import com.myproject.amqp.RabbitMQMessageProducer;
import com.myproject.clients.fraud.FraudCheckResponse;
import com.myproject.clients.fraud.FraudClient;
import com.myproject.clients.notification.NotificationRequest;
import com.myproject.customer.Customer;
import com.myproject.customer.CustomerRegistrationRequest;
import com.myproject.customer.repo.CustomerRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public record CustomerService(
        CustomerRepository customerRepository,
        RestTemplate restTemplate,
        FraudClient fraudClient,
        RabbitMQMessageProducer rabbitMQMessageProducer
) {
    public Customer registerCustomer(CustomerRegistrationRequest request) {
        Customer customer = Customer
                .builder()
                .firstName(request.firstName())
                .lastName(request.lastName())
                .email(request.email())
                .build();

        customerRepository.saveAndFlush(customer);

        FraudCheckResponse fraudCheckResponse = fraudClient.isFraudster(customer.getId());

        assert fraudCheckResponse != null;
        if (fraudCheckResponse.isFraudster()) {
            throw new IllegalStateException("fraudster");
        }

        NotificationRequest notificationRequest = new NotificationRequest(customer.getId(), customer.getEmail(),
                String.format("Hi %s, welcome to My-Microservices...",
                        customer.getFirstName()));

        rabbitMQMessageProducer.publish(
                notificationRequest,
                "internal.exchange",
                "internal.notification.routing-key"
        );

        return customer;
    }
}
